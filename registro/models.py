from django.db import models

# Create your models here.

class Empresa(models.Model):
    nombre = models.CharField(max_length=100)
    rubro = models.CharField(max_length=100)
    edad = models.IntegerField()
    def __str__(self):
        return "nombre: "+self.nombre+" rubro: "+self.rubro

