from django.shortcuts import render
from django.http import HttpResponse
from .models import Empresa
# Create your views here.

def index(request):
    return render(request, 'index.html',{'nombre':"Vicente",'elementos':["uno","dos","tres"]})

def registro(request):
    return render(request,'formulario.html',{})

def crear(request):
    nombre = request.POST.get('nombre','')
    rubro = request.POST.get('rubro','')
    rut = request.POST.get('rut',0)
    empresa = Empresa(nombre=nombre,rubro=rubro,rut=rut)
    empresa.save()
    return HttpResponse("nombre : "+nombre+" rubro: "+rubro)